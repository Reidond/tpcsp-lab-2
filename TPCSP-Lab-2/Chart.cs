﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace TPCSP_Lab_2
{
    public partial class Chart : Form
    {
        int[,] matrix;
        public Chart(int[,] matrix)
        {
            this.matrix = matrix;
            InitializeComponent();
        }

        private void FormChart_Load(object sender, EventArgs e)
        {
            var c = new int[(matrix.GetLength(0) + matrix.GetLength(1)) - 1];
            matrixChart.Series.Clear();
            var series1 = new Series("Matrix");
            for (int i = 0; i < c.Length; i++)
            {
                for (int row = 0; row < matrix.GetLength(0); row++)
                {
                    for (int col = 0; col < matrix.GetLength(1); col++)
                    {
                        if (matrix[row, col] > 0 && (row + col) <= i)
                            c[i]++;
                    }
                }
                series1.Points.AddXY(i, c[i]);
            }

            matrixChart.Series.Add(series1);
        }
    }
}
