﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPCSP_Lab_2
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void LoadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                using (var reader = new StreamReader(openFileDialog1.FileName))
                {
                    matrixDataGridView1.Rows.Clear();
                    var nums = reader.ReadLine().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    CheckForWord(ref nums);
                    matrixDataGridView1.ColumnCount = nums.Length;
                    matrixDataGridView1.Rows.Add(nums);
                    while (!reader.EndOfStream)
                    {
                        nums = reader.ReadLine().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        CheckForWord(ref nums);
                        matrixDataGridView1.Rows.Add(nums);
                    }
                }
        }

        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Matrix text file|*.txt";
            saveFileDialog1.Title = "Save matrix file";
            saveFileDialog1.ShowDialog();

            // If the file name is not an empty string open it for saving.  
            if (saveFileDialog1.FileName != "")
            {
                // File type selected in the dialog box.  
                // NOTE that the FilterIndex property is one-based.  
                switch (saveFileDialog1.FilterIndex)
                {
                    case 1:
                        string element;
                        TextWriter textWriter = new StreamWriter(saveFileDialog1.FileName);
                        for (int i = 0; i < matrixDataGridView1.RowCount - 1; i++)
                        {
                            for (int j = 0; j < matrixDataGridView1.ColumnCount; j++)
                            {
                                element = matrixDataGridView1.Rows[i].Cells[j].Value == null
                                    ? "0 "
                                    : matrixDataGridView1.Rows[i].Cells[j].Value.ToString() + " ";

                                if (CheckForWord(element).Parsed)
                                    element = "0 ";

                                textWriter.Write(element);
                            }
                            textWriter.WriteLine();
                        }
                        textWriter.Close();
                        break;
                }
            }
        }

        private void MatrixDataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void ColorizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < matrixDataGridView1.RowCount - 1; i++)
                ColorizeCell(matrixDataGridView1.Rows[i].Cells[i], Color.Green, Color.White);

            for (int i = 0; i < matrixDataGridView1.RowCount - 1; ++i)
            {
                for (int j = 0; j < matrixDataGridView1.ColumnCount; ++j)
                {
                    if (i < j)
                        ColorizeCell(matrixDataGridView1.Rows[i].Cells[j], Color.Blue, Color.White);
                    else if (i > j)
                        ColorizeCell(matrixDataGridView1.Rows[i].Cells[j], Color.Yellow, Color.Black);
                }
            }
        }

        private void UncolorizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < matrixDataGridView1.RowCount - 1; ++i)
                for (int j = 0; j < matrixDataGridView1.ColumnCount; ++j)
                    ColorizeCell(matrixDataGridView1.Rows[i].Cells[j]);
        }

        private void ShowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var matrix = DataGridToMatrix(matrixDataGridView1);
            Chart formChart = new Chart(matrix);
            formChart.Show();
        }

        private int[,] DataGridToMatrix(DataGridView dataGridView1)
        {
            var matrix = new int[dataGridView1.RowCount - 1, dataGridView1.ColumnCount];
            foreach (DataGridViewRow i in dataGridView1.Rows)
            {
                if (i.IsNewRow) continue;
                foreach (DataGridViewCell j in i.Cells)
                {
                    matrix[j.RowIndex, j.ColumnIndex] = Convert.ToInt32(j.Value);
                }
            }
            return matrix;
        }

        private void ColorizeCell(DataGridViewCell cell, Color backColor, Color foreColor)
        {
            cell.Style.BackColor = backColor;
            cell.Style.ForeColor = foreColor;
        }

        private void ColorizeCell(DataGridViewCell cell)
        {
            cell.Style.BackColor = Color.White;
            cell.Style.ForeColor = Color.Black;
        }

        private void CheckForWord(ref string[] nums)
        {
            for (int i = 0; i < nums.Length; i++)
                if (!int.TryParse(nums[i], out _))
                    nums[i] = "0";
        }

        private (bool Parsed, int Result) CheckForWord(string num)
        {
            if (int.TryParse(num, out int result))
                return (false, result);
            return (true, 0);
        }
    }
}
